﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrocodileGenerator : MonoBehaviour {
    public GameObject crocodilePrefab;

	// Use this for initialization
	void Start () {
        InvokeRepeating("GenRock", 3, 2);
		
	}
    void GenRock()
    {
        Instantiate(crocodilePrefab, new Vector3( 5, -3, 0), Quaternion.identity);
    }
	
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public GameObject explosionPrefab;

	
	// Update is called once per frame
	void Update () {
        transform.Translate(0.2f, 0, 0);

        if(transform.position.x > 5)
        {
            Destroy(gameObject);
        }
		
	}
    private void OnTriggerEnter2D(Collider2D collision)

    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        
        Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}

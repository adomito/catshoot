﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrocodileController : MonoBehaviour {
    float fallSpeed=0;
    //float rotSpeed;


	// Use this for initialization
	void Start () {
        this.fallSpeed = 0.01f + 0.1f * Random.value;
       // this.rotSpeed = 5f + 3f * Random.value;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(-fallSpeed, 0,0);
      

        if (transform.position.x < -5.5f)
        {
            Destroy(gameObject);
        }
		
	}
}
